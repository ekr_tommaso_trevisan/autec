@echo off
echo.

::set CDOLD=%CD%
::set CD=C:\EKR-PE_Clienti\Autec\Creazione_EKBS
set CDOLD=%CD%
cd /d %~dp0
echo.
del %CD%\Output_EKB\*.xml
del %CD%\Temp\*.xml
echo.
echo ***** ELABORAZIONE EKB *****
echo.
echo Step 1
echo.
%CD%\Eseguibile_S1\Mapping.exe > Logs\Log_elaborazione.log
echo.
echo Step 2
echo.
%CD%\Eseguibile_S2\Mapping.exe >> Logs\Log_elaborazione.log
echo.
echo Step 3
echo.
%CD%\Eseguibile_S3\Mapping.exe >> Logs\Log_elaborazione.log
echo.
echo Step 4
echo.
%CD%\Eseguibile_S4\Mapping.exe >> Logs\Log_elaborazione.log
echo.
echo Step 5
echo.
%CD%\Eseguibile_S5\Mapping.exe >> Logs\Log_elaborazione.log
echo.
echo Step 6
echo.
%CD%\Eseguibile_S6\Mapping.exe >> Logs\Log_elaborazione.log
echo.
echo Step 7
echo.
%CD%\Eseguibile_S7\Mapping.exe >> Logs\Log_elaborazione.log
echo.
echo ***** CARICAMENTO EKB ****
"C:\Program Files (x86)\EKR\Author\bin\EKRPE.Imports.Exports.exe" -f %CD%\Output_EKB\ -in >> Logs\Log_elaborazione.log
echo.

echo OK - %date% %time% > Logs\Esito_Caricamento_Ekb.txt
echo.

::set CD=%CDOLD%
cd /d %CDOLD%
echo.
pause