@echo off
echo.

::set CDOLD=%CD%
::set CD=C:\EKR-PE_Clienti\Autec\Creazione_EKBS
::set CDOLD=%CD%
cd /d %~dp0
echo.
del %CD%\Output_EKB\*.xml
::del %CD%\Temp\*.xml
echo.
echo ***** ELABORAZIONE EKB *****
echo.
echo Step 1
echo.
Eseguibile_S1\Mapping.exe
echo.
echo Step 2
echo.
Eseguibile_S2\Mapping.exe
echo.
echo Step 3
echo.
Eseguibile_S3\Mapping.exe
echo.
echo Step 4
echo.
Eseguibile_S4\Mapping.exe
echo.
echo Step 5
echo.
Eseguibile_S5\Mapping.exe
echo.
echo Step 6
echo.
Eseguibile_S6\Mapping.exe
echo.
echo Step 7
echo.
Eseguibile_S7\Mapping.exe
echo.


echo.

::set CD=%CDOLD%
cd /d %CDOLD%
echo.
pause