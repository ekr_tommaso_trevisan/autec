@echo off
echo.

rem Da togliere, solo per debug
rem GOTO INIZIO

REM STATE_DB_LOCK > Database bloccato, non possono essere caricate in access nuove specifiche da elaborare > i dati caricati posso essere elaborati
REM STATE_DB_UNLOCK > Database sbloccato, � possibile caricare in access nuove specifiche per le elaborazioni
REM STATE_PUB_COMPLETE > L'elaborazione EKB, gerarchia e selector � ferma, quindi pu� essere lanciata
REM STATE_PUB_RUNNING > L'elaborazione EKB, gerarchia e selector � in funzione, quindi non sono possibili altre elaborazioni


IF EXIST P:\Database\STATE_DB_LOCK IF EXIST P:\Database\STATE_PUB_COMPLETE GOTO INIZIO

GOTO FINE



:INIZIO
echo Inizio %date% %time%

echo.
set CDOLD=%CD%
::set CD=C:\EKR-PE_Clienti\Autec\Creazione_EKBS
cd /d %~dp0
echo.
del %CD%\Output_EKB\*.xml
echo.
echo ***** ELABORAZIONE EKB *****
echo.
echo Step 1
echo.
%CD%\Eseguibile_S1\Mapping.exe > Logs\Log_elaborazione.log
echo.
REM Sblocca il database access per consentire il caricamento di nuove specifiche da elaborare, e blocca la pubblicazione
ren P:\Database\STATE_DB_LOCK STATE_DB_UNLOCK
ren P:\Database\STATE_PUB_COMPLETE STATE_PUB_RUNNING
echo.
echo Step 2
echo.
%CD%\Eseguibile_S2\Mapping.exe >> Logs\Log_elaborazione.log
echo.
echo Step 3
echo.
%CD%\Eseguibile_S3\Mapping.exe >> Logs\Log_elaborazione.log
echo.
echo Step 4
echo.
%CD%\Eseguibile_S4\Mapping.exe >> Logs\Log_elaborazione.log
echo.
echo Step 5
echo.
%CD%\Eseguibile_S5\Mapping.exe >> Logs\Log_elaborazione.log
echo.
echo Step 6
echo.
%CD%\Eseguibile_S6\Mapping.exe >> Logs\Log_elaborazione.log
echo.
echo Step 7
echo.
%CD%\Eseguibile_S7\Mapping.exe >> Logs\Log_elaborazione.log
echo.
echo ***** CARICAMENTO EKB ****
"C:\Program Files (x86)\EKR\Author\bin\EKRPE.Imports.Exports.exe" -f %CD%\Output_EKB\ -in >> Logs\Log_elaborazione.log
echo.
ECHO.
echo %date% %time% > Logs\Esito_Caricamento_Ekb.txt
echo.

echo.
echo ***** ELABORAZIONE GERARCHIA *****
echo.
%CD%\Eseguibile_Gerarchia_S10\Mapping.exe >> Logs\Log_elaborazione.log
echo.
echo ***** CARICAMENTO GERARCHIA ****
echo.
%CD%\EKR.DataSources.Excel.Refresh\EKR.DataSources.Excel.Refresh.exe -dir %CD%\Output_Gerarchia\

del C:\EKR-PE\utility_DB\GERARCHIA_Files_x_import\*.xlsx

copy %CD%\Output_Gerarchia\*.xlsx C:\EKR-PE\utility_DB\GERARCHIA_Files_x_import\ >> Logs\Log_elaborazione.log

"C:\Program Files (x86)\EKR\Author\bin\EKRPE.Imports.Exports.exe" -ger -in >> Logs\Log_elaborazione.log

echo.
echo.
echo %date% %time% > Logs\Esito_Caricamento_Gerarchia.txt
echo.

echo ***** ESECUZIONE SELECTOR ****
"EKR Selector.lnk" -procedure PAFePubblica -procedureConf "P:\Settaggi_comuni_specifiche\settaggi\Selector\procedure\PAFePubblica.config" -closeOnError true >> Logs\Log_elaborazione.log
echo.
ren P:\Database\STATE_PUB_RUNNING STATE_PUB_COMPLETE
echo.

echo Fine %date% %time%



:FINE
::set CD=%CDOLD%
cd /d %CDOLD%
echo.
::pause