@echo off
echo.
::set CDOLD=%CD%
::set CD=C:\EKR-PE_Clienti\Autec\Creazione_EKBS
set CDOLD=%CD%
cd /d %~dp0
echo.
echo ***** ELABORAZIONE GERARCHIA *****
echo.
%CD%\Eseguibile_Gerarchia_S10\Mapping.exe >> Logs\Log_elaborazione.log
echo.
echo ***** CARICAMENTO GERARCHIA ****
echo.
%CD%\EKR.DataSources.Excel.Refresh\EKR.DataSources.Excel.Refresh.exe -dir %CD%\Output_Gerarchia\
del C:\EKR-PE\utility_DB\GERARCHIA_Files_x_import\*.xlsx
copy %CD%\Output_Gerarchia\*.xlsx C:\EKR-PE\utility_DB\GERARCHIA_Files_x_import\ >> Logs\Log_elaborazione.log
"C:\Program Files (x86)\EKR\Author\bin\EKRPE.Imports.Exports.exe" -ger -in >> Logs\Log_elaborazione.log
echo.
ECHO.
echo %date% %time% > Logs\Esito_Caricamento_Gerarchia.txt
echo.

::set CD=%CDOLD%
cd /d %CDOLD%
echo.
pause



